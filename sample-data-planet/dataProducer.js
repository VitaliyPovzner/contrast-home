require("dotenv").config();
const AWS = require("aws-sdk");
AWS.config.update({
  region: "us-east-1",
});
const service = require("./functions/functions");
const firehouse = new AWS.Firehose();

let dataChunks = service.getPreparetDataToSend(process.env.SOURCE_FILE);

function sendDataChunks(dataChunks) {
  for (let i = 0; i < dataChunks.length; i++) {
    const params = {
      Records: dataChunks[i],
      DeliveryStreamName: process.env.KINESIS_DATA_STREAM,
    };
    firehouse
      .putRecordBatch(params)
      .promise()
      .then(
        (response) => {
          console.log(
            "populated firehose with " +
              response.RequestResponses.length +
              " records"
          );
        },
        (err) => console.error(err.stack)
      );
  }
}
sendDataChunks(dataChunks);
