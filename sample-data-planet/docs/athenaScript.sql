CREATE EXTERNAL TABLE planetSample (
creationdate bigint,
planet string,
value double) 
ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.OpenCSVSerde' 
WITH SERDEPROPERTIES ("separatorChar" = ",", "escapeChar" = "\\") 
LOCATION 's3://planet-data-vp/2022/'