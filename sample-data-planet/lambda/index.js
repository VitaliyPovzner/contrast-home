const aws = require("aws-sdk");
aws.config.update({
  region: "us-east-1",
});

const AthenaExpress = require("athena-express");
const athenaExpress = new AthenaExpress({
  aws,
  s3: process.env.S3OUTPUT,
  db: process.env.DB,
});
exports.handler = async (event) => {
  const from = event.queryStringParameters.from;
  const to = event.queryStringParameters.to;
  const type = event.pathParameters.proxy.substr(
    0,
    event.pathParameters.proxy.indexOf("/")
  );
  let result;
  try {
    result = await athenaExpress.query(
      `select AVG (value) as value, COUNT(*) as processedcount from planet where planet='${type}' and creationdate BETWEEN (${from}) AND (${to}) `
    );
    result = result.Items[0];
    result.type = type;
    const response = {
      statusCode: 200,
      body: JSON.stringify(result),
    };
    return response;
  } catch (e) {
    console.log(e.stack);
    const response = {
      statusCode: 500,
      body: JSON.stringify("an errror has occured"),
    };
    return response;
  }
};
