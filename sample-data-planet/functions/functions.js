const fs = require("fs");
const os = require("os");

module.exports = {
  getDataFromFileSync: function (file) {
    try {
      data = fs.readFileSync(file, "utf8");
      return data;
    } catch (err) {
      console.error(err);
    }
  },
  formatRecordsForFireHose: function (data) {
    return data.split(os.EOL).reduce((acc, currentString) => {
      const record = { Data: currentString + "\n" };
      acc.push(record);
      return acc;
    }, []);
  },
  chunkArrayInGroups: function (arr, size) {
    let myArray = [];
    for (let i = 0; i < arr.length; i += size) {
      myArray.push(arr.slice(i, i + size));
    }
    return myArray;
  },
  getPreparetDataToSend(file) {
    let data = this.getDataFromFileSync(file);
    data = this.formatRecordsForFireHose(data);
    data = this.chunkArrayInGroups(data, 500);
    return data;
  },
};
